%global apiver 2.4
%define release_version %(echo %{version} | awk -F. '{print $1"."$2}')

%global glib2_version 2.61.2
%global libsigc_version 2.9.1

Name:           glibmm24
Version:        2.66.7
Release:        1
Summary:        C++ interface for the GLib library
License:        LGPLv2+
URL:            http://www.gtkmm.org/
Source0:        http://ftp.gnome.org/pub/GNOME/sources/glibmm/%{release_version}/glibmm-%{version}.tar.xz
Patch0:         %{name}-gcc11.patch

BuildRequires:  gcc-c++ glib2-devel >=  %{glib2_version}  libsigc++20-devel >= %{libsigc_version}
BuildRequires:  m4 perl-generators perl-interpreter perl-Getopt-Long
BuildRequires:  meson doxygen libxslt mm-common graphviz

Requires:       glib2%{?_isa} >= %{glib2_version} libsigc++20%{?_isa} >= %{libsigc_version}

# Do not export private Perl modules
%global __provides_exclude %{?__provides_exclude:%{__provides_exclude}|}^perl\\(
%global __requires_exclude %{?__requires_exclude:%{__requires_exclude}|}^perl\\((DocsParser|Enum|Function|FunctionBase|GtkDefs|Object|Output|Property|Util|WrapParser)\\)

%description
The glibmm package is a set of C++ bindings for Glib.
It provides non-UI API that is not available in standard C++
and makes it possible for gtkmm to wrap GObject-based APIs.

%package devel
Summary:        Headers for developing programs using glibmm24
Requires:       %{name} = %{version}-%{release}

%description devel
The glibmm24-devel package contains the static libraries and
header files needed for developing glibmm applications.

%package        help
Provides:       %{name}-doc = %{version}-%{release}
Obsoletes:      %{name}-doc < %{version}-%{release}
Summary:        full API documentation for glibmm24
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}
Requires:       libsigc++20-doc

%description    help
The glibmm24-help package contains the full API documentation for glibmm24.

%prep
%autosetup -p1 -n glibmm-%{version}

%build
%meson -Dbuild-documentation=true
%meson_build

%install
%meson_install
%delete_la
chmod +x $RPM_BUILD_ROOT%{_libdir}/glibmm-%{apiver}/proc/generate_wrap_init.pl
chmod +x $RPM_BUILD_ROOT%{_libdir}/glibmm-%{apiver}/proc/gmmproc

%files
%license COPYING
%{_libdir}/libgiomm-%{apiver}.so.1*
%{_libdir}/libglibmm-%{apiver}.so.1*
%{_libdir}/libglibmm_generate_extra_defs-%{apiver}.so.1*

%files devel
%{_includedir}/glibmm-%{apiver}/
%{_includedir}/giomm-%{apiver}/
%{_libdir}/*.so
%{_libdir}/glibmm-%{apiver}/
%{_libdir}/giomm-%{apiver}/
%{_libdir}/pkgconfig/*.pc

%files help
%doc NEWS
%doc %{_datadir}/devhelp/
%doc %{_docdir}/glibmm-%{apiver}/

%changelog
* Fri Jan 17 2025 yaoxin <1024769339@qq.com> - 2.66.7-1
- Update to 2.66.7:
  * Gio::content_type_guess(): Remove most of an unneccesary overload
  * NMake Makefiles: Use GLIB_INCLUDEDIR instead of GLIB_BASE_INCLUDEDIR
  * NMake Makefiles: Make dep paths configurable
  * subprojects/sigc++-2.0.wrap: Get the libsigc++-2-12 branch
  * tools/defs_gen/h2def.py: Make return types that are unsigned work
  * GdkDmabufTextureBuilder contains functions that return unsigned int.
  * Gio::File: Fix various spelling errors in .hg docs
  * Doxyfile.in: Remove obsolete entries
  * tests: Test for /etc/passwd instead of /etc/fstab

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 2.66.5-1
- Update to 2.66.5

* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 2.66.4-1
- Update to 2.66.4

* Fri Jun 18 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 2.64.5-1
- Upgrade to 2.64.5
- Use meson rebuild

* Thu Dec 12 2019 zoushuangshuang<zoushuangshuang@huawei.com> - 2.58.0-2
- Package init
